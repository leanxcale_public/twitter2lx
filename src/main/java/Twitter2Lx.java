import com.leanxcale.kivi.database.Database;
import com.leanxcale.kivi.database.Field;
import com.leanxcale.kivi.database.Table;
import com.leanxcale.kivi.database.Type;
import com.leanxcale.kivi.session.Session;
import com.leanxcale.kivi.session.SessionFactory;
import com.leanxcale.kivi.session.Settings;
import com.leanxcale.kivi.tuple.Tuple;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collections;
import twitter4j.FilterQuery;
import twitter4j.Status;
import twitter4j.StatusAdapter;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Example class to streaming twitter content to LeanXcale database
 */
public class Twitter2Lx {

  // Endpoint to LeanXcale database (replace localhost with your host address)
  private static final String SERVER_URL = "lx://localhost:2181/twitterdb@AP";

  // Table name
  private static final String TABLE_NAME = "TWEETS";

  // Column names, used to create the table and insert tuples.
  private static final String ID_FIELD = "id";
  private static final String USER_FIELD = "username";
  private static final String CREATED_FIELD = "created";
  private static final String TEXT_FIELD = "text";
  private static final String RT_FIELD = "rt";
  private static final String LAT_FIELD = "lat";
  private static final String LON_FIELD = "lon";
  private static final String PLACE_FIELD = "place";

  public static void main(String[] args){


    try{

      // First we define the settings to connect to the database
      Settings settings = Settings.parse(SERVER_URL);

      // Open a session
      Session session = SessionFactory.newSession(settings);

      // Get the table (the first time is created)
      Table table = getOrCreateTable(session);

      // Configure twitter stream
      ConfigurationBuilder cb = new ConfigurationBuilder();
      cb.setDebugEnabled(true);


      TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
      twitterStream.addListener(new LeanXcaleListener(table, session));

      // Open the stream
      twitterStream.sample();

      // Filter the stream (we use "datascience" as key word and english as language)
      twitterStream.filter(new FilterQuery("datascience").language("en"));

      // Add shoutdown hook to close the session and the stream in a friendly way
      Runtime.getRuntime().addShutdownHook(
          new Thread(() -> {
            twitterStream.shutdown();
            session.close();
          }));
    }
    catch (Exception e){

      e.printStackTrace();
    }
  }

  /**
   * Convert from tweet to table tuple
   *
   * @param status Tweet object
   * @param table Table which will store the tweets (is needed to get the proper tuple format)
   *
   * @return Tuple object to be stored
   */
  private static Tuple tweetToTuple(Status status, Table table){

    // Create an empty tuple using the table template
    Tuple tuple = table.createTuple();

    // Fills the tuple fields from tweet attributes
    tuple.put(ID_FIELD, status.getId());
    tuple.put(USER_FIELD, status.getUser().getName());
    tuple.put(CREATED_FIELD, new Timestamp(status.getCreatedAt().getTime()));
    tuple.put(TEXT_FIELD, status.getText());
    tuple.put(RT_FIELD, status.getRetweetCount());

    // Fills the geo data if available
    if(status.getGeoLocation() != null){

      tuple.put(LAT_FIELD, status.getGeoLocation().getLatitude());
      tuple.put(LON_FIELD, status.getGeoLocation().getLongitude());
    }

    // Fills the place data if available
    if(status.getPlace() != null){

      tuple.put(PLACE_FIELD, status.getPlace().getFullName());
    }

    return tuple;
  }

  /**
   * Retrieves the tweets table. If does not exists creates it.
   *
   * @param session Leanxcale session
   *
   * @return Table object representing the tweets table.
   */
  private static Table getOrCreateTable(Session session){

    // Gets the database object (to access the schema)
    Database database = session.database();


    // Checks the existence of the table
    if(!database.tableExists(TABLE_NAME)) {

      // If the table does not exist creates it.
      return database.createTable(
          TABLE_NAME,                                                 // Table name
          Collections.singletonList(new Field(ID_FIELD, Type.LONG)),  // Key fields
          Arrays.asList(                                              // Rest of the fields
              new Field(USER_FIELD, Type.STRING),
              new Field(CREATED_FIELD, Type.TIMESTAMP),
              new Field(TEXT_FIELD, Type.STRING),
              new Field(RT_FIELD, Type.INT),
              new Field(LAT_FIELD, Type.DOUBLE),
              new Field(LON_FIELD, Type.DOUBLE),
              new Field(PLACE_FIELD, Type.STRING)
          )
      );
    }

    // If the table exists, retrieves it from the database
    return database.getTable(TABLE_NAME);
  }

  /**
   * Static class which acts as a stream listener, converting the tweets to tuple, and storing them
   */
  private static class LeanXcaleListener extends StatusAdapter {

    // To group the inserts within a single commit
    int commitCount;

    Table table;
    Session session;

    LeanXcaleListener(Table table, Session session) {
      this.table = table;
      this.session = session;
      this.commitCount = 0;
    }

    @Override
    public void onStatus(Status status) {

      try {

        // Converts the tweet to a tuple
        Tuple tuple = tweetToTuple(status, table);

        // Insert the tuple (uses upsert to avoid PK check)
        table.upsert(tuple);

        System.out.println("Inserting: " + tuple);

        commitCount++;

        // If the commit group is complete, do the commit
        if(commitCount > 5){

          commitCount = 0;
          session.commit();
        }
      }
      // Exception handling
      catch (Exception e){

        e.printStackTrace();

        try {

          session.rollback();
        }
        catch (Exception e1){

          e1.printStackTrace();
        }
      }
    }
  }
}
